import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { LanguageCode } from '../../config/language-code.enum';
import { Translation } from '../../base/locale-types';
import { Product } from './product.entity';

@Entity()
export class ProductTranslation implements Translation<Product> {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Index()
  languageCode: LanguageCode;

  @Column('varchar')
  name: string;

  @Column('text')
  description: string;

  @ManyToOne(() => Product, (base) => base.translations, {
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  base: Product;
}

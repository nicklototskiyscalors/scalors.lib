import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Video } from './video.entity';

@Entity()
export class VideoAvailability extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column('int')
  startTime: number;

  @ApiProperty()
  @Column('int')
  endTime: number;

  @ManyToOne(() => Video, (video) => video.availabilities, {
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  video: Video;
}

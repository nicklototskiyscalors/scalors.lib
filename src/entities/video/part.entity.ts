import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Expose, Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity, Translatable } from '../../base';
import { PartTranslation } from './part-translation.entity';
import { PartChapter } from './part-chapter.entity';
import { PartVideoDocument } from './part-video-document.entity';
import { Product } from '../product';

export enum PartSize {
  Small = 'small',
  Big = 'big',
}

@Entity()
export class Part extends BaseEntity implements Translatable {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column('text')
  thumbnail: string;

  @ApiProperty({ name: 'number' })
  @Column({ type: 'varchar', length: 32 })
  partNumber: string;

  @Column({ type: 'varchar', length: 32 })
  dimensions: string;

  @ApiProperty()
  @Column({
    type: 'enum',
    enum: PartSize,
  })
  size: PartSize;

  @ApiProperty()
  @Expose()
  get name(): string {
    return this.translations?.[0].name;
  }

  @ApiProperty()
  @Expose()
  get quantity(): number {
    const relation: { quantity: number; is_all: boolean }[] =
      this.partVideoDocuments || this.partChapters;

    const isAllQty = relation?.find(rel => rel?.is_all)
    if (isAllQty?.quantity) {
      return isAllQty.quantity;
    }

    return relation?.reduce((sum, current) => sum + current.quantity, 0);
  }

  @Transform(() => undefined, { toPlainOnly: true })
  @OneToMany(() => PartChapter, (partChapter) => partChapter.part)
  partChapters: PartChapter[];

  @Transform(() => undefined, { toPlainOnly: true })
  @OneToMany(
    () => PartVideoDocument,
    (partVideoDocument) => partVideoDocument.part,
  )
  partVideoDocuments: PartVideoDocument[];

  @ManyToOne(() => Product, (product) => product.parts)
  product: Product;

  @OneToMany(() => PartTranslation, (translation) => translation.base)
  @Transform(() => undefined, { toPlainOnly: true })
  translations: PartTranslation[];
}

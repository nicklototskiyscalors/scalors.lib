import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { LanguageCode } from '../../config/language-code.enum';
import { Translation } from '../../base/locale-types';
import { TipTopic } from './tip-topic.entity';

@Entity()
export class TipTopicTranslation implements Translation<TipTopic> {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Index()
  languageCode: LanguageCode;

  @Column('varchar')
  name: string;

  @ManyToOne(() => TipTopic, (base) => base.translations, {
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  base: TipTopic;
}

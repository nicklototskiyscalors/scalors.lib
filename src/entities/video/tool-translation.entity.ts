import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { LanguageCode } from '../../config/language-code.enum';
import { Translation } from '../../base/locale-types';
import { Tool } from './tool.entity';

@Entity()
export class ToolTranslation implements Translation<Tool> {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Index()
  languageCode: LanguageCode;

  @Column('varchar')
  name: string;

  @Column('text')
  description: string;

  @ManyToOne(() => Tool, (base) => base.translations, {
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  base: Tool;
}

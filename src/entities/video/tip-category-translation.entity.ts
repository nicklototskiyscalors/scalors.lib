import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { LanguageCode } from '../../config/language-code.enum';
import { Translation } from '../../base/locale-types';
import { TipCategory } from './tip-category.entity';

@Entity()
export class TipCategoryTranslation implements Translation<TipCategory> {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Index()
  languageCode: LanguageCode;

  @Column('varchar')
  name: string;

  @ManyToOne(() => TipCategory, (base) => base.translations, {
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  base: TipCategory;
}

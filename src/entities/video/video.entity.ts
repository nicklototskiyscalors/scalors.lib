import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Product } from '../product';
import { VideoAvailability } from './video-availability.entity';

export enum VideoPerspective {
  Main = 'main',
  CloseLook = 'closeLook',
  TopShot = 'topShot',
  WideAngle = 'wideAngle',
}

@Entity()
@Unique(['perspective', 'product'])
export class Video extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column('varchar')
  source: string;

  @ApiProperty({ type: 'enum', enum: VideoPerspective })
  @Column({
    type: 'enum',
    enum: VideoPerspective,
    nullable: true,
  })
  perspective: VideoPerspective;

  @ManyToOne(() => Product, (product) => product.videos)
  product: Product;

  @ApiProperty()
  @OneToMany(() => VideoAvailability, (availability) => availability.video)
  availabilities: VideoAvailability[];
}

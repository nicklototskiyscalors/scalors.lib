import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Expose, Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Translatable } from '../../base/locale-types';
import { Tip } from './tip.entity';
import { TipTopicTranslation } from './tip-topic-translation.entity';

@Entity()
export class TipTopic extends BaseEntity implements Translatable {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Expose()
  get name(): string {
    return this.translations?.[0].name;
  }

  @ApiProperty()
  numberOfTips: number;

  @OneToMany(() => Tip, (tip) => tip.topic)
  tips: Tip[];

  @OneToMany(() => TipTopicTranslation, (translation) => translation.base)
  @Transform(() => undefined, { toPlainOnly: true })
  translations: TipTopicTranslation[];
}

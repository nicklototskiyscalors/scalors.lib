import {
  Column,
  Entity,
  Index,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude, Expose, Transform } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { ChapterTranslation } from './chapter-translation.entity';
import { BaseEntity } from '../../base/base.entity';
import { Translatable } from '../../base/locale-types';
import { Tool } from './tool.entity';
import { PartChapter } from './part-chapter.entity';
import { VideoDocument } from './video-document.entity';
import { ConstructionTime } from './construction-time.entity';
import { Tip } from './tip.entity';
import { Product } from '../product';

export enum DifficultyLevel {
  Low = 'low',
  Middle = 'middle',
  High = 'high',
}

@Entity()
export class Chapter extends BaseEntity implements Translatable {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column('text')
  thumbnail: string;

  @ApiProperty()
  @Column('int')
  startTime: number;

  @ApiProperty()
  @Column('int')
  endTime: number;

  @ApiProperty()
  @Column({ type: 'int', default: 0 })
  numberOfPeople: number;

  @ApiProperty()
  @Column({
    type: 'enum',
    enum: DifficultyLevel,
    default: DifficultyLevel.Middle,
  })
  difficultyLevel: DifficultyLevel;

  @ApiProperty()
  @Column({ type: 'boolean', default: false })
  orderDependency: boolean;

  @ApiProperty()
  @Expose()
  get name(): string {
    return this.translations?.[0].name;
  }

  @ApiProperty()
  numberOfDocuments: number;

  @ApiProperty()
  numberOfTips: number;

  @ManyToMany(() => VideoDocument, (videoDocument) => videoDocument.chapters)
  @JoinTable({ name: 'chapter_video_document' })
  videoDocuments: VideoDocument[];

  @OneToMany(() => PartChapter, (partChapter) => partChapter.chapter)
  partChapters: PartChapter[];

  @ManyToOne(() => Product, (product) => product.chapters)
  product: Product;

  @ManyToMany(() => Tool, (tool) => tool.chapters)
  tools: Tool[];

  @OneToMany(() => ChapterTranslation, (translation) => translation.base)
  @Transform(() => undefined, { toPlainOnly: true })
  translations: ChapterTranslation[];

  @ApiPropertyOptional({ type: ConstructionTime, isArray: true })
  @ManyToMany(() => ConstructionTime, (cTime) => cTime.chapters)
  constructionTimes: ConstructionTime[];

  @ManyToMany(() => Tip, (tip) => tip.chapters)
  tips: Tip[];
}

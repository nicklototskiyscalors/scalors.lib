import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BaseEntity } from '../../base/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Chapter } from './chapter.entity';
import { VideoDocument } from './video-document.entity';

export enum ConstructionType {
  Beginner = 'beginner',
  Handyman = 'handyman',
  Professional = 'professional',
}

@Entity()
export class ConstructionTime extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ type: 'enum', enum: ConstructionType })
  @Column({ type: 'enum', enum: ConstructionType })
  type: ConstructionType;

  @ApiProperty()
  @Column('int')
  duration: number;

  @ManyToMany(() => Chapter, (chapter) => chapter.constructionTimes)
  @JoinTable({ name: 'chapter_construction_time' })
  chapters: Chapter[];

  @ManyToMany(
    () => VideoDocument,
    (videoDocument) => videoDocument.constructionTimes,
  )
  @JoinTable({ name: 'video_document_construction_time' })
  videoDocuments: VideoDocument[];
}

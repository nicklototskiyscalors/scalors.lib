import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Expose, Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Translatable } from '../../base/locale-types';
import { TipTranslation } from './tip-translation.entity';
import { TipCategory } from './tip-category.entity';
import { TipTopic } from './tip-topic.entity';
import { VideoDocument } from './video-document.entity';
import { Chapter } from './chapter.entity';
import { Product } from '../product';

@Entity()
export class Tip extends BaseEntity implements Translatable {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ type: 'varchar', nullable: true })
  audio: string;

  @ApiProperty()
  @Expose()
  get text(): string {
    return this.translations?.[0].text;
  }

  @ApiProperty()
  @Column({ type: 'varchar', nullable: true })
  video_src: string;

  @ApiProperty()
  @Expose()
  get name(): string {
    return this.translations?.[0].name;
  }

  @ApiProperty()
  @ManyToOne(() => TipCategory, (tipCategory) => tipCategory.tips)
  category: TipCategory;

  @ManyToOne(() => TipTopic, (tipTopic) => tipTopic.tips)
  topic: TipTopic;

  @ApiProperty({ name: 'videoDocument', type: VideoDocument })
  @ManyToMany(() => VideoDocument, (videoDocument) => videoDocument.tips)
  @JoinTable({ name: 'tip_video_document' })
  @Expose({ name: 'videoDocument' })
  @Transform(({ value }) => value[0], { toPlainOnly: true })
  videoDocuments: VideoDocument[];

  @ManyToMany(() => Chapter, (chapter) => chapter.tips)
  @JoinTable({ name: 'tip_chapter' })
  chapters: Chapter[];

  @ManyToOne(() => Product, (product) => product.tips)
  product: Product;

  @OneToMany(() => TipTranslation, (translation) => translation.base)
  @Transform(() => undefined, { toPlainOnly: true })
  translations: TipTranslation[];
}

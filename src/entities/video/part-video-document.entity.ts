import { Column, Entity, ManyToOne } from 'typeorm';
import { Part } from './part.entity';
import { VideoDocument } from './video-document.entity';

@Entity()
export class PartVideoDocument {
  @Column({ type: 'int', default: 0 })
  quantity: number;

  @Column({ type: 'boolean', default: false })
  is_all: boolean;

  @ManyToOne(() => Part, (part) => part.partVideoDocuments, {
    primary: true,
    onDelete: 'CASCADE',
  })
  public part: Part;

  @ManyToOne(
    () => VideoDocument,
    (videoDocument) => videoDocument.partVideoDocuments,
    {
      primary: true,
      onDelete: 'CASCADE',
    },
  )
  public videoDocument!: VideoDocument;
}

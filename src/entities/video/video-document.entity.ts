import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Tool } from './tool.entity';
import { Chapter } from './chapter.entity';
import { Tip } from './tip.entity';
import { ConstructionTime } from './construction-time.entity';
import { PartVideoDocument } from './part-video-document.entity';
import { Product } from '../product';

@Entity()
export class VideoDocument extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column('text')
  thumbnail: string;

  @ApiProperty()
  @Column('text')
  source: string;

  @ApiProperty()
  @Column({ type: 'int', default: 0 })
  page: number;

  @ApiProperty()
  @Column('int')
  startTime: number;

  @ApiProperty()
  @Column('int')
  endTime: number;

  @ApiProperty()
  numberOfTips: number;

  @ManyToMany(() => Tool, (tool) => tool.videoDocuments)
  tools: Tool[];

  @ManyToMany(() => Chapter, (chapter) => chapter.videoDocuments)
  chapters: Chapter[];

  @ManyToMany(() => Tip, (tip) => tip.videoDocuments)
  tips: Tip[];

  @ManyToOne(() => Product, (product) => product.videoDocuments)
  product: Product;

  @ApiPropertyOptional({ type: ConstructionTime, isArray: true })
  @ManyToMany(() => ConstructionTime, (cTime) => cTime.videoDocuments)
  constructionTimes: ConstructionTime[];

  @OneToMany(
    () => PartVideoDocument,
    (partVideoDoc) => partVideoDoc.videoDocument,
  )
  partVideoDocuments: PartVideoDocument[];
}

/**
 * Entities which have localizable string properties should implement this type.
 */
import { BaseEntity } from './base.entity';

export interface Translatable {
  translations: Array<Translation<BaseEntity>>;
}

/**
 * Translations of localizable entities should implement this type.
 */
export type Translation<T> = {
  id: number;
  languageCode: string;
  base: T;
};

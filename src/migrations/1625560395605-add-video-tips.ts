import { MigrationInterface, QueryRunner } from 'typeorm';

export class addVideoTips1625560395605 implements MigrationInterface {
  name = 'addVideoTips1625560395605';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tip" ADD "video_src" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "tip" DROP COLUMN "video_src"`);
  }
}
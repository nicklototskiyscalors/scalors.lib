import { MigrationInterface, QueryRunner } from 'typeorm';

export class createConstructionTimePerVideoDocument1621492757579
  implements MigrationInterface
{
  name = 'createConstructionTimePerVideoDocument1621492757579';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "construction_time" DROP CONSTRAINT "FK_1ec7e790bae25afdfde319c1eb1"`,
    );
    await queryRunner.query(
      `CREATE TABLE "chapter_construction_time" ("construction_time_id" integer NOT NULL, "chapter_id" integer NOT NULL, CONSTRAINT "PK_5ca66a53e66cc2922f076b5e9c4" PRIMARY KEY ("construction_time_id", "chapter_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_465c0210bdbdeb1525a6945d97" ON "chapter_construction_time" ("construction_time_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_cc3b1cee3971f4533609a3c90f" ON "chapter_construction_time" ("chapter_id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "video_document_construction_time" ("construction_time_id" integer NOT NULL, "video_document_id" integer NOT NULL, CONSTRAINT "PK_1491aec5c4dc8cac43b3e965255" PRIMARY KEY ("construction_time_id", "video_document_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_285797847b1b7658f9daba5188" ON "video_document_construction_time" ("construction_time_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_fa9c4b773b84423b8816c5a8f5" ON "video_document_construction_time" ("video_document_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "construction_time" DROP COLUMN "chapter_id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter_construction_time" ADD CONSTRAINT "FK_465c0210bdbdeb1525a6945d97e" FOREIGN KEY ("construction_time_id") REFERENCES "construction_time"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter_construction_time" ADD CONSTRAINT "FK_cc3b1cee3971f4533609a3c90fd" FOREIGN KEY ("chapter_id") REFERENCES "chapter"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document_construction_time" ADD CONSTRAINT "FK_285797847b1b7658f9daba51880" FOREIGN KEY ("construction_time_id") REFERENCES "construction_time"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document_construction_time" ADD CONSTRAINT "FK_fa9c4b773b84423b8816c5a8f54" FOREIGN KEY ("video_document_id") REFERENCES "video_document"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "video_document_construction_time" DROP CONSTRAINT "FK_fa9c4b773b84423b8816c5a8f54"`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document_construction_time" DROP CONSTRAINT "FK_285797847b1b7658f9daba51880"`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter_construction_time" DROP CONSTRAINT "FK_cc3b1cee3971f4533609a3c90fd"`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter_construction_time" DROP CONSTRAINT "FK_465c0210bdbdeb1525a6945d97e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "construction_time" ADD "chapter_id" integer`,
    );
    await queryRunner.query(`DROP INDEX "IDX_fa9c4b773b84423b8816c5a8f5"`);
    await queryRunner.query(`DROP INDEX "IDX_285797847b1b7658f9daba5188"`);
    await queryRunner.query(`DROP TABLE "video_document_construction_time"`);
    await queryRunner.query(`DROP INDEX "IDX_cc3b1cee3971f4533609a3c90f"`);
    await queryRunner.query(`DROP INDEX "IDX_465c0210bdbdeb1525a6945d97"`);
    await queryRunner.query(`DROP TABLE "chapter_construction_time"`);
    await queryRunner.query(
      `ALTER TABLE "construction_time" ADD CONSTRAINT "FK_1ec7e790bae25afdfde319c1eb1" FOREIGN KEY ("chapter_id") REFERENCES "chapter"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}

import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTableForPerspective1627378620336
  implements MigrationInterface
{
  name = 'createTableForPerspective1627378620336';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "video_availability" ("id" SERIAL NOT NULL, "start_time" integer NOT NULL, "end_time" integer NOT NULL, "video_id" integer, CONSTRAINT "PK_85d85ac240c425718a96c8372c4" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_availability" ADD CONSTRAINT "FK_736872089dd2d0dd4bfe36e0e5d" FOREIGN KEY ("video_id") REFERENCES "video"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `INSERT INTO "video_availability" ("id", "start_time", "end_time", "video_id") VALUES
            (1, 0, 1948, 2),
            (2, 0, 304, 1),
            (3, 315, 650, 1),
            (4, 672, 684, 1),
            (5, 691, 727, 1),
            (6, 738, 778, 1),
            (7, 789, 1947, 1),
            (8, 0, 314, 4),
            (9, 330, 1369, 4),
            (10, 1423, 1572, 4),
            (11, 1579, 1597, 4),
            (12, 1666, 1947, 4),
            (13, 0, 304, 3),
            (14, 330, 1572, 3),
            (15, 1579, 1947, 3);`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "video_availability" DROP CONSTRAINT "FK_736872089dd2d0dd4bfe36e0e5d"`,
    );
    await queryRunner.query(`DROP TABLE "video_availability"`);
  }
}

import { MigrationInterface, QueryRunner } from 'typeorm';

export class removeUselessTablesAndColumns1626772886903
  implements MigrationInterface
{
  name = 'removeUselessTablesAndColumns1626772886903';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE IF EXISTS "video_source"`);
    await queryRunner.query(`DROP TABLE IF EXISTS "subtitle"`);
    await queryRunner.query(`ALTER TABLE "video" DROP COLUMN "longevity"`);
    await queryRunner.query(`ALTER TABLE "video" DROP COLUMN "creation"`);
    await queryRunner.query(`ALTER TABLE "video" DROP COLUMN "product"`);
    await queryRunner.query(`ALTER TABLE "video" DROP COLUMN "preview"`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "video" ADD "preview" text NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "video" ADD "product" integer NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "video" ADD "creation" integer NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "video" ADD "longevity" numeric NOT NULL`,
    );
  }
}

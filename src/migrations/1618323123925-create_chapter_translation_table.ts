import { MigrationInterface, QueryRunner } from 'typeorm';

export class createChapterTranslationTable1618323123925
  implements MigrationInterface
{
  name = 'createChapterTranslationTable1618323123925';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "chapter_translation" ("id" SERIAL NOT NULL, "language_code" character varying NOT NULL, "name" character varying NOT NULL, "base_id" integer, CONSTRAINT "PK_a0f1cb3808f7eda704a2a8ecd0e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_fcac91405cb571253329834586" ON "chapter_translation" ("language_code") `,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter_translation" ADD CONSTRAINT "FK_96a261d9834eb2edbb26c19c42f" FOREIGN KEY ("base_id") REFERENCES "chapter"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "chapter_translation" DROP CONSTRAINT "FK_96a261d9834eb2edbb26c19c42f"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_fcac91405cb571253329834586"`);
    await queryRunner.query(`DROP TABLE "chapter_translation"`);
  }
}

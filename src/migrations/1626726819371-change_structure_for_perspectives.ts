import { MigrationInterface, QueryRunner } from 'typeorm';

export class changeStructureForPerspectives1626726819371
  implements MigrationInterface
{
  name = 'changeStructureForPerspectives1626726819371';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "IDX_bdbece297b7b401b475676b59b"`);
    await queryRunner.query(`DROP INDEX "IDX_83a5315c3498e5708786094b19"`);
    await queryRunner.query(`DROP INDEX "IDX_9769e5e31c1906083b7d2502da"`);
    await queryRunner.query(
      `ALTER TABLE "tip" RENAME COLUMN "video_id" TO "product_id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document" RENAME COLUMN "video_id" TO "product_id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" RENAME COLUMN "video_id" TO "product_id"`,
    );
    await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "video_id"`);
    await queryRunner.query(`ALTER TABLE "part" ADD "product_id" integer`);
    await queryRunner.query(`ALTER TABLE "tool" ADD "product_id" integer`);
    await queryRunner.query(`ALTER TABLE "video" ADD "product_id" integer`);
    await queryRunner.query(
      `ALTER TABLE "tip" ALTER COLUMN "product_id" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document" ALTER COLUMN "product_id" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" ALTER COLUMN "product_id" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip" ADD CONSTRAINT "FK_3d37fad420be397a4ea869460e2" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "part" ADD CONSTRAINT "FK_4702a686e977ef750811fb3f457" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document" ADD CONSTRAINT "FK_0f3337bcaa9b491e078160a1cb8" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool" ADD CONSTRAINT "FK_046adf92d678bfa2364a772c0d4" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" ADD CONSTRAINT "FK_c9bb2164b95cb02c6d9f53dbc1d" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "video" ADD CONSTRAINT "FK_cef1355c04b55793cae16006a42" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query('Update "part" set "product_id"=1');
    await queryRunner.query('Update "tool" set "product_id"=1');
    await queryRunner.query('Update "video" set "product_id"=1');
    await queryRunner.query('Update "tip" set "product_id"=1');
    await queryRunner.query('Update "video_document" set "product_id"=1');
    await queryRunner.query('Update "chapter" set "product_id"=1');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "video" DROP CONSTRAINT "FK_cef1355c04b55793cae16006a42"`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" DROP CONSTRAINT "FK_c9bb2164b95cb02c6d9f53dbc1d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool" DROP CONSTRAINT "FK_046adf92d678bfa2364a772c0d4"`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document" DROP CONSTRAINT "FK_0f3337bcaa9b491e078160a1cb8"`,
    );
    await queryRunner.query(
      `ALTER TABLE "part" DROP CONSTRAINT "FK_4702a686e977ef750811fb3f457"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip" DROP CONSTRAINT "FK_3d37fad420be397a4ea869460e2"`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" ALTER COLUMN "product_id" SET NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document" ALTER COLUMN "product_id" SET NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip" ALTER COLUMN "product_id" SET NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "video" DROP COLUMN "product_id"`);
    await queryRunner.query(`ALTER TABLE "video" DROP COLUMN "source"`);
    await queryRunner.query(`ALTER TABLE "tool" DROP COLUMN "product_id"`);
    await queryRunner.query(`ALTER TABLE "part" DROP COLUMN "product_id"`);
    await queryRunner.query(
      `ALTER TABLE "product" ADD "video_id" integer NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" RENAME COLUMN "product_id" TO "video_id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_document" RENAME COLUMN "product_id" TO "video_id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip" RENAME COLUMN "product_id" TO "video_id"`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_9769e5e31c1906083b7d2502da" ON "product" ("video_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_83a5315c3498e5708786094b19" ON "chapter" ("video_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_bdbece297b7b401b475676b59b" ON "video_document" ("video_id") `,
    );
  }
}

import { MigrationInterface, QueryRunner } from 'typeorm';

export class moveQuantityFieldToPartChapterTable1619692041586
  implements MigrationInterface
{
  name = 'moveQuantityFieldToPartChapterTable1619692041586';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "IDX_41bca8758e31937a6582fa0d77"`);
    await queryRunner.query(`DROP INDEX "IDX_44fa764c296ca40246e77ea3a8"`);
    await queryRunner.query(`ALTER TABLE "part" DROP COLUMN "quantity"`);
    await queryRunner.query(
      `ALTER TABLE "part_chapter" ADD "quantity" integer NOT NULL DEFAULT 0`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "part_chapter" DROP COLUMN "quantity"`,
    );
    await queryRunner.query(
      `ALTER TABLE "part" ADD "quantity" integer NOT NULL`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_44fa764c296ca40246e77ea3a8" ON "part_chapter" ("chapter_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_41bca8758e31937a6582fa0d77" ON "part_chapter" ("part_id") `,
    );
  }
}

import { MigrationInterface, QueryRunner } from 'typeorm';

export class createProductTable1619529328896 implements MigrationInterface {
  name = 'createProductTable1619529328896';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "product" ("id" SERIAL NOT NULL, "video_id" integer NOT NULL, "product_number" character varying NOT NULL, "program" character varying NOT NULL, "thumbnail" text NOT NULL, CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_9769e5e31c1906083b7d2502da" ON "product" ("video_id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "product_translation" ("id" SERIAL NOT NULL, "language_code" character varying NOT NULL, "name" character varying NOT NULL, "description" text NOT NULL, "base_id" integer, CONSTRAINT "PK_62d00fbc92e7a495701d6fee9d5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_d428c9feaa30b9efc46e081d3d" ON "product_translation" ("language_code") `,
    );
    await queryRunner.query(
      `ALTER TABLE "product_translation" ADD CONSTRAINT "FK_95f5b9d70a95981ab6d0be7b73a" FOREIGN KEY ("base_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "product_translation" DROP CONSTRAINT "FK_95f5b9d70a95981ab6d0be7b73a"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_d428c9feaa30b9efc46e081d3d"`);
    await queryRunner.query(`DROP TABLE "product_translation"`);
    await queryRunner.query(`DROP INDEX "IDX_9769e5e31c1906083b7d2502da"`);
    await queryRunner.query(`DROP TABLE "product"`);
  }
}

import {MigrationInterface, QueryRunner} from "typeorm";

export class simplifyVideos1624962742948 implements MigrationInterface {
    name = 'simplifyVideos1624962742948'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "video" ALTER COLUMN "source" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "video" ALTER COLUMN "product" SET NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "video" ALTER COLUMN "product" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "video" ALTER COLUMN "source" DROP NOT NULL`);
    }

}
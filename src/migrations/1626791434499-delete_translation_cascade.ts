import {MigrationInterface, QueryRunner} from "typeorm";

export class deleteTranslationCascade1626791434499 implements MigrationInterface {
    name = 'deleteTranslationCascade1626791434499'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "chapter_translation" DROP CONSTRAINT "FK_96a261d9834eb2edbb26c19c42f"`);
        await queryRunner.query(`ALTER TABLE "tip_translation" DROP CONSTRAINT "FK_6ad5ac37808dd3254c1738ea3be"`);
        await queryRunner.query(`ALTER TABLE "tip_category_translation" DROP CONSTRAINT "FK_ab4752c92f0f77dbfcbefe7eefa"`);
        await queryRunner.query(`ALTER TABLE "tip_topic_translation" DROP CONSTRAINT "FK_33f929de5a65d64c2d9e468f53d"`);
        await queryRunner.query(`ALTER TABLE "part_translation" DROP CONSTRAINT "FK_fbad730c9202486183e46a43a96"`);
        await queryRunner.query(`ALTER TABLE "tool_translation" DROP CONSTRAINT "FK_301358e0be128210752f49ccd20"`);
        await queryRunner.query(`ALTER TABLE "product_translation" DROP CONSTRAINT "FK_95f5b9d70a95981ab6d0be7b73a"`);
        await queryRunner.query(`ALTER TABLE "chapter_translation" ADD CONSTRAINT "FK_96a261d9834eb2edbb26c19c42f" FOREIGN KEY ("base_id") REFERENCES "chapter"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tip_translation" ADD CONSTRAINT "FK_6ad5ac37808dd3254c1738ea3be" FOREIGN KEY ("base_id") REFERENCES "tip"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tip_category_translation" ADD CONSTRAINT "FK_ab4752c92f0f77dbfcbefe7eefa" FOREIGN KEY ("base_id") REFERENCES "tip_category"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tip_topic_translation" ADD CONSTRAINT "FK_33f929de5a65d64c2d9e468f53d" FOREIGN KEY ("base_id") REFERENCES "tip_topic"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "part_translation" ADD CONSTRAINT "FK_fbad730c9202486183e46a43a96" FOREIGN KEY ("base_id") REFERENCES "part"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tool_translation" ADD CONSTRAINT "FK_301358e0be128210752f49ccd20" FOREIGN KEY ("base_id") REFERENCES "tool"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "product_translation" ADD CONSTRAINT "FK_95f5b9d70a95981ab6d0be7b73a" FOREIGN KEY ("base_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product_translation" DROP CONSTRAINT "FK_95f5b9d70a95981ab6d0be7b73a"`);
        await queryRunner.query(`ALTER TABLE "tool_translation" DROP CONSTRAINT "FK_301358e0be128210752f49ccd20"`);
        await queryRunner.query(`ALTER TABLE "part_translation" DROP CONSTRAINT "FK_fbad730c9202486183e46a43a96"`);
        await queryRunner.query(`ALTER TABLE "tip_topic_translation" DROP CONSTRAINT "FK_33f929de5a65d64c2d9e468f53d"`);
        await queryRunner.query(`ALTER TABLE "tip_category_translation" DROP CONSTRAINT "FK_ab4752c92f0f77dbfcbefe7eefa"`);
        await queryRunner.query(`ALTER TABLE "tip_translation" DROP CONSTRAINT "FK_6ad5ac37808dd3254c1738ea3be"`);
        await queryRunner.query(`ALTER TABLE "chapter_translation" DROP CONSTRAINT "FK_96a261d9834eb2edbb26c19c42f"`);
        await queryRunner.query(`ALTER TABLE "product_translation" ADD CONSTRAINT "FK_95f5b9d70a95981ab6d0be7b73a" FOREIGN KEY ("base_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tool_translation" ADD CONSTRAINT "FK_301358e0be128210752f49ccd20" FOREIGN KEY ("base_id") REFERENCES "tool"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "part_translation" ADD CONSTRAINT "FK_fbad730c9202486183e46a43a96" FOREIGN KEY ("base_id") REFERENCES "part"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tip_topic_translation" ADD CONSTRAINT "FK_33f929de5a65d64c2d9e468f53d" FOREIGN KEY ("base_id") REFERENCES "tip_topic"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tip_category_translation" ADD CONSTRAINT "FK_ab4752c92f0f77dbfcbefe7eefa" FOREIGN KEY ("base_id") REFERENCES "tip_category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tip_translation" ADD CONSTRAINT "FK_6ad5ac37808dd3254c1738ea3be" FOREIGN KEY ("base_id") REFERENCES "tip"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "chapter_translation" ADD CONSTRAINT "FK_96a261d9834eb2edbb26c19c42f" FOREIGN KEY ("base_id") REFERENCES "chapter"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}

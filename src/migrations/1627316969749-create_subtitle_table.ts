import { MigrationInterface, QueryRunner } from 'typeorm';

export class createSubtitleTable1627316969749 implements MigrationInterface {
  name = 'createSubtitleTable1627316969749';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "subtitle" ("id" SERIAL NOT NULL, "source" character varying NOT NULL, "label" character varying NOT NULL, "product_id" integer, CONSTRAINT "UQ_f530ef2952e88da7c7a51c722c1" UNIQUE ("label", "product_id"), CONSTRAINT "PK_994ad1599c74d6da447883869b5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "subtitle" ADD CONSTRAINT "FK_5bab64381e8661e55a6d63dbfd9" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `INSERT INTO "subtitle" ("source", "label", "product_id") VALUES ('https://wb-videos-syo.s3.eu-central-1.amazonaws.com/stage/0/subtitles/Rollcontainer+FINAL_EN.vtt', 'English', '1');`,
    );
    await queryRunner.query(
      `INSERT INTO "subtitle" ("source", "label", "product_id") VALUES ('https://wb-videos-syo.s3.eu-central-1.amazonaws.com/stage/0/subtitles/Rollcontainer+FINAL_DE.vtt', 'Deutsch', '1');`,
    );
    await queryRunner.query(
      `INSERT INTO "subtitle" ("source", "label", "product_id") VALUES ('https://wb-videos-syo.s3.eu-central-1.amazonaws.com/stage/0/subtitles/Rollcontainer+FINAL_FR.vtt', 'Français', '1');`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "subtitle" DROP CONSTRAINT "FK_5bab64381e8661e55a6d63dbfd9"`,
    );
    await queryRunner.query(`DROP TABLE "subtitle"`);
  }
}

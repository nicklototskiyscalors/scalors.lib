import {MigrationInterface, QueryRunner} from "typeorm";

export class addParspectiveColumn1626728124912 implements MigrationInterface {
    name = 'addParspectiveColumn1626728124912'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "video_perspective_enum" AS ENUM('main', 'closeLook', 'topShot', 'wideAngle')`);
        await queryRunner.query(`ALTER TABLE "video" ADD "perspective" "video_perspective_enum"`);
        await queryRunner.query(`ALTER TABLE "video" ADD CONSTRAINT "UQ_fc95690feac2ebd604a7da10fb2" UNIQUE ("perspective", "product_id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "video" DROP CONSTRAINT "UQ_fc95690feac2ebd604a7da10fb2"`);
        await queryRunner.query(`ALTER TABLE "video" DROP COLUMN "perspective"`);
        await queryRunner.query(`DROP TYPE "video_perspective_enum"`);
    }

}

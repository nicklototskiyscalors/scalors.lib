import { MigrationInterface, QueryRunner } from 'typeorm';

export class createVideoDocumentTable1618599506825
  implements MigrationInterface
{
  name = 'createVideoDocumentTable1618599506825';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "video_document" ("id" SERIAL NOT NULL, "video_id" integer NOT NULL, "thumbnail" text NOT NULL, "source" text NOT NULL, "start_time" integer NOT NULL, "end_time" integer NOT NULL, CONSTRAINT "PK_8971b07182a99ed0c3d09347cab" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_bdbece297b7b401b475676b59b" ON "video_document" ("video_id") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "IDX_bdbece297b7b401b475676b59b"`);
    await queryRunner.query(`DROP TABLE "video_document"`);
  }
}

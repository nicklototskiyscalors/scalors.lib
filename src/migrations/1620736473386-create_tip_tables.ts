import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTipTables1620736473386 implements MigrationInterface {
  name = 'createTipTables1620736473386';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "tip_translation" ("id" SERIAL NOT NULL, "language_code" character varying NOT NULL, "name" character varying NOT NULL, "text" text NOT NULL, "base_id" integer, CONSTRAINT "PK_779783095644c880fc764ac49b8" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_d60184a90bb3f5f53785ae45cc" ON "tip_translation" ("language_code") `,
    );
    await queryRunner.query(
      `CREATE TABLE "tip_category_translation" ("id" SERIAL NOT NULL, "language_code" character varying NOT NULL, "name" character varying NOT NULL, "base_id" integer, CONSTRAINT "PK_3949126bcf80c7f9898610ecd8d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_292f8c5ab0dadbd9064853f878" ON "tip_category_translation" ("language_code") `,
    );
    await queryRunner.query(
      `CREATE TABLE "tip_category" ("id" SERIAL NOT NULL, CONSTRAINT "PK_caa03046fa3bb4bcb3c5b17e1da" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "tip_topic_translation" ("id" SERIAL NOT NULL, "language_code" character varying NOT NULL, "name" character varying NOT NULL, "base_id" integer, CONSTRAINT "PK_41b634ae933927cafea5876dc37" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_89efd8d18d014fd2788973aecc" ON "tip_topic_translation" ("language_code") `,
    );
    await queryRunner.query(
      `CREATE TABLE "tip_topic" ("id" SERIAL NOT NULL, CONSTRAINT "PK_9575c31c8b3b5050ad264220e4d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "tip" ("id" SERIAL NOT NULL, "video_id" integer NOT NULL, "audio" character varying, "category_id" integer, "topic_id" integer, CONSTRAINT "PK_855d736988802b4ec0e07b7e762" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "tip_video_document" ("tip_id" integer NOT NULL, "video_document_id" integer NOT NULL, CONSTRAINT "PK_11512a08ee1d844147477a1331a" PRIMARY KEY ("tip_id", "video_document_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_9a88d440123d005677d8221644" ON "tip_video_document" ("tip_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_ec80f98d26384d492e0ba06ac4" ON "tip_video_document" ("video_document_id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "tip_chapter" ("tip_id" integer NOT NULL, "chapter_id" integer NOT NULL, CONSTRAINT "PK_ffa9f6429a2ee77adb220f90e85" PRIMARY KEY ("tip_id", "chapter_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_9e131aaa8f024b317ef1ca08a0" ON "tip_chapter" ("tip_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_23fb7e3e65b24ff39d630fea23" ON "tip_chapter" ("chapter_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_translation" ADD CONSTRAINT "FK_6ad5ac37808dd3254c1738ea3be" FOREIGN KEY ("base_id") REFERENCES "tip"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_category_translation" ADD CONSTRAINT "FK_ab4752c92f0f77dbfcbefe7eefa" FOREIGN KEY ("base_id") REFERENCES "tip_category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_topic_translation" ADD CONSTRAINT "FK_33f929de5a65d64c2d9e468f53d" FOREIGN KEY ("base_id") REFERENCES "tip_topic"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip" ADD CONSTRAINT "FK_caa03046fa3bb4bcb3c5b17e1da" FOREIGN KEY ("category_id") REFERENCES "tip_category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip" ADD CONSTRAINT "FK_9575c31c8b3b5050ad264220e4d" FOREIGN KEY ("topic_id") REFERENCES "tip_topic"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_video_document" ADD CONSTRAINT "FK_9a88d440123d005677d8221644c" FOREIGN KEY ("tip_id") REFERENCES "tip"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_video_document" ADD CONSTRAINT "FK_ec80f98d26384d492e0ba06ac46" FOREIGN KEY ("video_document_id") REFERENCES "video_document"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_chapter" ADD CONSTRAINT "FK_9e131aaa8f024b317ef1ca08a03" FOREIGN KEY ("tip_id") REFERENCES "tip"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_chapter" ADD CONSTRAINT "FK_23fb7e3e65b24ff39d630fea23b" FOREIGN KEY ("chapter_id") REFERENCES "chapter"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tip_chapter" DROP CONSTRAINT "FK_23fb7e3e65b24ff39d630fea23b"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_chapter" DROP CONSTRAINT "FK_9e131aaa8f024b317ef1ca08a03"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_video_document" DROP CONSTRAINT "FK_ec80f98d26384d492e0ba06ac46"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_video_document" DROP CONSTRAINT "FK_9a88d440123d005677d8221644c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip" DROP CONSTRAINT "FK_9575c31c8b3b5050ad264220e4d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip" DROP CONSTRAINT "FK_caa03046fa3bb4bcb3c5b17e1da"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_topic_translation" DROP CONSTRAINT "FK_33f929de5a65d64c2d9e468f53d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_category_translation" DROP CONSTRAINT "FK_ab4752c92f0f77dbfcbefe7eefa"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tip_translation" DROP CONSTRAINT "FK_6ad5ac37808dd3254c1738ea3be"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_23fb7e3e65b24ff39d630fea23"`);
    await queryRunner.query(`DROP INDEX "IDX_9e131aaa8f024b317ef1ca08a0"`);
    await queryRunner.query(`DROP TABLE "tip_chapter"`);
    await queryRunner.query(`DROP INDEX "IDX_ec80f98d26384d492e0ba06ac4"`);
    await queryRunner.query(`DROP INDEX "IDX_9a88d440123d005677d8221644"`);
    await queryRunner.query(`DROP TABLE "tip_video_document"`);
    await queryRunner.query(`DROP TABLE "tip"`);
    await queryRunner.query(`DROP TABLE "tip_topic"`);
    await queryRunner.query(`DROP INDEX "IDX_89efd8d18d014fd2788973aecc"`);
    await queryRunner.query(`DROP TABLE "tip_topic_translation"`);
    await queryRunner.query(`DROP TABLE "tip_category"`);
    await queryRunner.query(`DROP INDEX "IDX_292f8c5ab0dadbd9064853f878"`);
    await queryRunner.query(`DROP TABLE "tip_category_translation"`);
    await queryRunner.query(`DROP INDEX "IDX_d60184a90bb3f5f53785ae45cc"`);
    await queryRunner.query(`DROP TABLE "tip_translation"`);
  }
}
